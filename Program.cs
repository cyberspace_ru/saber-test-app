﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SaberTestApp
{
    class Program
    {
        const string FILE_NAME = "serialized-list.data";

        static void Main(string[] args)
        {
            ListRand toSerialize = CreateListRand(10);
            toSerialize.Serialize(new FileStream(FILE_NAME, FileMode.Create));
            Console.WriteLine("toSerialize object: \n" + toSerialize.AsString());

            ListRand toDeserialize = new ListRand();
            toDeserialize.Deserialize(new FileStream(FILE_NAME, FileMode.Open));
            Console.WriteLine("\ntoDeserialize object: \n" + toDeserialize.AsString());

            Console.ReadLine();
        }

        // Объективно, удобнее создать метод Add в классе ListRand, но - "Нельзя изменять исходную структуру классов ListNode, ListRand."
        static ListRand CreateListRand(int count)
        {
            ListRand result = new ListRand();
            result.Count = count;
            if (count > 0)
            {
                List<ListNode> nodes = new List<ListNode>();
                for (int i = 0; i < count; i++)
                {
                    ListNode node = new ListNode();
                    node.Data = "element" + i;
                    if (i != 0)
                    {
                        node.Prev = nodes[i - 1];
                        nodes[i - 1].Next = node;
                    }
                    nodes.Add(node);
                }
                Random random = new Random(Guid.NewGuid().GetHashCode());
                nodes.ForEach(x => {
                    x.Rand = nodes[random.Next(nodes.Count)];
                });
                result.Head = nodes[0];
                result.Tail = nodes[nodes.Count - 1];
            }
            return result;
        }
    }
}