﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SaberTestApp
{
    /// Так как нельзя изменять структуру класса вынес некоторые методы.
    static class Extensions
    {
        public static List<ListNode> ToList(this ListRand self)
        {
            List<ListNode> result = new List<ListNode>();
            ListNode node = self.Head;
            while (node != null)
            {
                result.Add(node);
                node = node.Next;
            }
            return result;
        }

        // ListRand.ToString()
        public static string AsString(this ListRand self)
        {
            return String.Join("\n", self.ToList().Select(x => x.AsString()));
        }

        // ListNode.ToString()
        public static string AsString(this ListNode self)
        {
            return "Prev=" + (self.Prev != null ? self.Prev.Data : "null") +
                " current=" + self.Data +
                " Next=" + (self.Next != null ? self.Next.Data : "null") +
                " Rand=" + (self.Rand != null ? self.Rand.Data : "null");
        } 
    }
}
