﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SaberTestApp
{
    class ListRand
    {
        // Небезопасно, необходимо использовать свойства с приватным акссесором Set. Изменения которых возможно только через метод Add.
        // Так как нельзя изменять структуру класса, то метод Add, так же не реализован.
        public ListNode Head;
        public ListNode Tail;
        public int Count; // Так же небезопасно.

        // В логике сереализации/десереализации я опускаю проверку целостности списка, которая может быть нарушена из-за нарушения инкапсуляции.
        public void Serialize(FileStream s)
        {
            // Некорректно в одном месте сериализовать и приводить данную структуру к словарю. Нарушаю принцип единственной ответственности.
            // Но по причине описанной выше я это делаю.
            Dictionary<ListNode, Guid> dictionary = new Dictionary<ListNode, Guid>();
            ListNode carret = Head;
            for (int i = 0; i < Count; i++)
            {
                dictionary.Add(carret, Guid.NewGuid());
                carret = carret.Next;
            }
            
            // Использую Dictionary, так как List.indexOf имеет O(n) сложность, а Dictionary[key] - O(1).
            
            using (BinaryWriter writer = new BinaryWriter(s))
            {
                writer.Write(Count);
                foreach (var pair in dictionary)
                {
                    writer.Write(pair.Value.ToString());
                    writer.Write(pair.Key.Data);
                }
                foreach (var pair in dictionary)
                {
                    writer.Write(dictionary[pair.Key.Rand].ToString());
                }
            }
        }

        public void Deserialize(FileStream s)
        {
            using (BinaryReader reader = new BinaryReader(s))
            {
                this.Count = reader.ReadInt32();
                if (this.Count > 0)
                {
                    Dictionary<Guid, ListNode> dictionary = new Dictionary<Guid, ListNode>();
                    ListNode prev = null;
                    for (int i = 0; i < this.Count; i++)
                    {
                        Guid guid = Guid.Parse(reader.ReadString());
                        ListNode node = new ListNode();
                        node.Data = reader.ReadString();
                        dictionary.Add(guid, node);
                        if (i == 0)
                            Head = node;
                        if (i == this.Count - 1)
                            Tail = node;
                        if (prev != null)
                        {
                            node.Prev = prev;
                            prev.Next = node;
                        }
                        prev = node;
                    }
                    foreach(var pair in dictionary)
                    {
                        Guid rand = Guid.Parse(reader.ReadString());
                        pair.Value.Rand = dictionary[rand];
                    }
                }
            }
        }
    }
}
