﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SaberTestApp
{
    class ListNode
    {
        public ListNode Prev;
        public ListNode Next;
        public ListNode Rand; // произвольный элемент внутри списка
        public string Data;
    }
}
